<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DompetStatus extends Model
{
    use HasFactory;

    protected $table = 'dompet_status';

    protected $fillable = [
        'name',
        'created_at',
        'updated_at',
    ];

    public function master_dompet()
    {
        return $this->hasOne(MDompet::class, 'status_id','id');
    }

}
