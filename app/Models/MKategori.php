<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MKategori extends Model
{
    use HasFactory;

    protected $table = 'm_kategori';

    protected $fillable = [
        'status_id',
        'name',
        'deskripsi',
        'created_at',
        'updated_at',
    ];

    public function kategori_status()
    {
        return $this->belongsTo(KategoriStatus::class, 'status_id','id');
    }

}
