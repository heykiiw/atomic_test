<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MDompet extends Model
{
    use HasFactory;

    protected $table = 'm_dompet';

    protected $fillable = [
        'status_id',
        'name',
        'referensi',
        'deskripsi',
        'created_at',
        'updated_at',
    ];

    public function dompet_status()
    {
        return $this->belongsTo(DompetStatus::class, 'status_id','id');
    }

}
