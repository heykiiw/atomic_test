@extends('layouts.master')

@push('title','Create Dompet')

@push('title-breadcrumb', 'Tambah Dompet')

@push('script-js-head')
@endpush

@section('content')
<!-- Row -->

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <nav>
                        <ul class="breadcrumb breadcrumb-pipe">
                            <li class="breadcrumb-item">Master</li>
                            <li class="breadcrumb-item">Dompet</li>
                            <li class="breadcrumb-item active">Create</li>
                        </ul>
                    </nav>
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">{{ __('Dompet') }}</h3>
                        </div>
                        @if (session('success'))
                            <div class="mt-2">
                                <div class="alert alert-success p-1">
                                    {{ session('success') }}
                                </div>
                            </div>
                        @endif

                        @if (session('error'))
                            <div class="mt-2">
                                <div class="alert alert-danger p-1">
                                    {{ session('error') }}
                                </div>
                            </div>
                        @endif
                        <div class="nk-block-head-content">
                            <div class="toggle-wrap nk-block-tools-toggle">
                                <a class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="pageMenu">
                                    <em class="icon ni ni-more-v"></em>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="nk-block nk-block-lg">
                    <div class="card">

                        <form action="{{URL('/master-dompet')}}" class="form-horizontal" method="POST">
                            @csrf
                            @method('POST')

                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label" for="full-name-1">Nama</label>
                                            <div class="form-control-wrap">
                                                <input name="nama" type="text" value="{{old('nama')}}"
                                                       onblur="checkLength(this)"
                                               class="form-control @if($errors->has('nama')) is-invalid @endif">
                                            </div>
                                            <span class="form-control-feedback text-right text-danger" style="display: none" id="namavalidasi">
                                                Nama minimal 5 karakter
                                            </span>
                                            @if($errors->has('nama'))
                                                <div class="form-control-feedback text-right text-danger">
                                                    {{ $errors->first('nama')}}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label" for="full-name-1">Referensi</label>
                                            <div class="form-control-wrap">
                                                <input name="referensi" type="text" value="{{old('referensi')}}" class="form-control @if($errors->has('referensi')) is-invalid @endif">
                                            </div>
                                            @if($errors->has('referensi'))
                                                <div class="form-control-feedback text-right text-danger">
                                                    {{ $errors->first('referensi')}}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12 mt-1">
                                        <div class="form-group">
                                            <label class="form-label" for="full-name-1">Deskripsi</label>
                                            <div class="form-control-wrap">
                                                <textarea name="deskripsi" onblur="checkDeskripsiLength(this)" class="form-control @if($errors->has('deskripsi')) is-invalid @endif">
                                                    {{old('deskripsi')}}
                                                </textarea>
                                            </div>
                                            <span class="form-control-feedback text-right text-danger" style="display: none" id="deskripsiValidasi">
                                                Nama minimal 5 karakter
                                            </span>
                                            @if($errors->has('deskripsi'))
                                                <div class="form-control-feedback text-right text-danger">
                                                    {{ $errors->first('deskripsi')}}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-1">
                                        <div class="form-group">
                                            <label class="form-label" for="full-name-1">Status</label>
                                            <div class="form-control-wrap">
                                                <select name="status" class="form-control">
                                                    @foreach($dompetStatus as $ds)
                                                        <option value="{{ $ds->id }}" {{(old('status')==$ds->id)? 'selected':''}}>{{ $ds->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            @if($errors->has('status'))
                                                <div class="form-control-feedback text-right text-danger">
                                                    {{ $errors->first('status')}}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>


                                <button type="submit" class="btn btn-info mt-5"> Simpan</button>
                                <a href="{{ URL('/master-dompet') }}" class="btn btn-warning mt-5"> Kembali</a>
                            </div>

                        </form>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

<script>
    function checkLength(el) {
        if (el.value.length < 5) {
            $("#namavalidasi").show();
        }else{
            $("#namavalidasi").hide();
        }
    }

    function checkDeskripsiLength(al) {
        if (al.value.length > 100) {
            $("#deskripsivalidasi").show();
        }else{
            $("#deskripsivalidasi").hide();
        }
    }
</script>
@push('script-js')
@endpush
